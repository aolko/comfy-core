def comfy_core():
    # Your wrapper logic here
    result1 = func1()
    result2 = func2()
    # Process results or perform additional operations


# Export only the wrapper function
__all__ = ['comfy_core']
