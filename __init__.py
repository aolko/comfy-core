from .wrapper import comfy_core

# Export only the wrapper function
__all__ = ['comfy_core']
