from setuptools import setup, find_packages

setup(
    name='comfy-core',
    version='1.0',
    packages=find_packages(),
    install_requires=[
        "torch",
        "torchsde",
        "einops",
        "transformers>=4.25.1",
        "safetensors>=0.3.0",
        "aiohttp",
        "accelerate",
        "pyyaml",
        "Pillow",
        "scipy",
        "tqdm",
        "psutil"
    ],
    author='aolko',
    description=''
)
